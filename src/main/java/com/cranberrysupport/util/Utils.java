package com.cranberrysupport.util;

import com.cranberrysupport.gui.NewRequeteFrame;

import java.io.*;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils {

    public static String sha256(String password){
        String result = "";
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] passwordBytes = password.getBytes("UTF-8");
            byte[] passwordBytesArray = digest.digest(passwordBytes);
            result = Base64.getEncoder().encodeToString(passwordBytesArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Create a copy of a file in dat/
     * @param fileSrc the path
     */
    public static void saveFile(File fileSrc) {
        try {
            File file = new File("dat/" + fileSrc.getName());
            InputStream srcFile = new FileInputStream(fileSrc);
            OutputStream newFile = new FileOutputStream(file);
            byte[] buf = new byte[4096];
            int len;
            while ((len = srcFile.read(buf)) > 0) {
                newFile.write(buf, 0, len);
            }
            srcFile.close();
            newFile.close();
        } catch (IOException ex) {
            Logger.getLogger(NewRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }












}
