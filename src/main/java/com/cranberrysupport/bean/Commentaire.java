package com.cranberrysupport.bean;

public class Commentaire {
	
    private String commentaire;
    private String auteur;

    public Commentaire(String commentaire, String auteur) {
        this.commentaire = commentaire;
        this.auteur = auteur;
    }

    public String getAuteur() {
        return auteur;
    }

    public String getComment() {
        return commentaire;
    }

    public String formattedComments() {
        return auteur + ": " + commentaire;
    }

    @Override
    public String toString() {
        return "Commentaire{" +
                "commentaire='" + commentaire + '\'' +
                ", auteur=" + auteur +
                '}';
    }
}