package com.cranberrysupport.bean;

public class Client extends Utilisateur {

    public Client(String nomUtilisateur, String mdp, String role) {
        super(nomUtilisateur, mdp, role);
    }

    @Override
    public String getRole() {
        return Utilisateur.ROLE_CLIENT;
    }
}