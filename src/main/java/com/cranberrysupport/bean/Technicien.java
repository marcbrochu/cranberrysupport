package com.cranberrysupport.bean;

public class Technicien extends Utilisateur {

    public Technicien(String nomUtilisateur, String mdp, String role) {
        super(nomUtilisateur, mdp, role);
    }

    @Override
    public String getRole() {
        return Utilisateur.ROLE_TECHNICIEN;
    }
}