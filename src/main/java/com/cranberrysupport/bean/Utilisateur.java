package com.cranberrysupport.bean;

public abstract class Utilisateur {

	public final static String ROLE_CLIENT = "client";
	public final static String ROLE_TECHNICIEN = "technicien";
	
    private String surnom;
    private String mdp;
    private String role;

    public Utilisateur(String surnom, String mdp, String role) {
        this.surnom = surnom;
        this.mdp = mdp;
        this.role = role;
    }

    public String getMdp() {
        return mdp;
    }

    public String getRole() {
        return role;
    }

    public String getSurnom() {
        return surnom;
    }
    
}