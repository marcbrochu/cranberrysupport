package com.cranberrysupport.bean;

import java.io.File;
import java.util.ArrayList;

public class Requete {

    public enum Statut {
        ouvert("ouvert"), enTraitement("en traitement"), finalAbandon("Abandon"), finalSucces("Succes");
    	
        String valeur;

        //Constructeur des statuts
        Statut(String s) {
            this.valeur = s;
        }

        public String getValueString() {
            return valeur;
        }

        //Retourne le statut en fonction d'une string donnee
        public static Statut fromString(String text) {
            if (text != null) {
                if (Statut.ouvert.getValueString().equals(text)) {
                    return Statut.ouvert;
                } else if (Statut.enTraitement.getValueString().equals(text)) {
                    return Statut.enTraitement;
                } else if (Statut.finalAbandon.getValueString().equals(text)) {
                    return Statut.finalAbandon;
                } else if (Statut.finalSucces.getValueString().equals(text)) {
                    return Statut.finalSucces;
                } else if (Statut.finalAbandon.getValueString().equals(text)) {
                    return Statut.finalAbandon;
                }
            }
            return null;
        }
    }

    public enum Categorie {
        posteDeTravail("Poste de travail"), serveur("Serveur"),
        serviceWeb("Service web"), compteUsager("Compte usager"), autre("Autre");
    	
        String value;

        Categorie(String s) {
            this.value = s;
        }

        public String getValueString() {
            return value;
        }

        //Retourne la Categorie en fonction d'une String donnee
        public static Categorie fromString(String text) {
            if (text != null) {
                if (Categorie.posteDeTravail.getValueString().equals(text)) {
                    return Categorie.posteDeTravail;
                } else if (Categorie.serveur.getValueString().equals(text)) {
                    return Categorie.serveur;
                } else if (Categorie.serviceWeb.getValueString().equals(text)) {
                    return Categorie.serviceWeb;
                } else if (Categorie.compteUsager.getValueString().equals(text)) {
                    return Categorie.compteUsager;
                } else if (Categorie.autre.getValueString().equals(text)) {
                    return Categorie.autre;
                }
            }
            return null;
        }
    }

    private String sujet;
    private String description;
    private File fichier;
    private Integer numero;
    private String clientUsername;
    private String techUsername;
    private Statut statut;
    private Categorie categorie;
    private ArrayList<Commentaire> listeCommentaires;

    public Requete(int numero, String sujet, String descrip, String clientUsername, Categorie categorie) {
        this.clientUsername = clientUsername;
        this.sujet = sujet;
        this.description = descrip;
        this.numero= numero;
        this.statut = Statut.ouvert;
        this.categorie = categorie;
        this.listeCommentaires = new ArrayList<>();
    }

    // Utiliser par SQL ->>> rename createRequeteFromSQL
    public Requete(String sujet, String description, String fichier, int numero, String clientUsername, String techUsername,
                   Statut statut, Categorie categorie, ArrayList<Commentaire> listeCommentaires) {
        this(numero, sujet, description, clientUsername, categorie);

        if (fichier != null)
            this.fichier = new File(fichier);
        this.clientUsername = clientUsername;
        this.techUsername = techUsername;
        this.statut = statut;
        if (listeCommentaires != null)
            this.listeCommentaires = listeCommentaires;
    }

    public void addCommentaire(String message, String surnom) {
        listeCommentaires.add(new Commentaire(message, surnom));
    }

    /**Setters**/

    // Il n'y a pas de Technicien au depart a moins qu'il la cree lui-meme
    // A un certain point il faut donc designer un technicien
    public void setTech(Utilisateur tech) {
        if (tech != null) {
            if (tech.getRole().equals(Utilisateur.ROLE_TECHNICIEN)) {
                this.techUsername = tech.getSurnom();
            }
        }
    }

    public void setFile(File file) {
        fichier = file;
    }

    public void setStatut(Statut newstat) {
        statut = newstat;
    }

    public void setCategorie(Categorie choix) {
        categorie = choix;
    }

    /**Getters**/
    public String getSujet() {
        return sujet;
    }

    public String getDescription() {
        return description;
    }

    public File getFichier() {
        return fichier;
    }

    public Integer getNumero() {
        return numero;
    }

    public String getClient() {
        return clientUsername;
    }

    public Statut getStatut() {
        return statut;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public ArrayList<Commentaire> getCommentaires() {
        return listeCommentaires;
    }

    /** SQL Getters **/
    public String getFichierPath() {
        if (fichier != null)
            return fichier.toString();
        return "";
    }

    public String getTechnicien() {
        if (techUsername != null)
            return techUsername;
        return "";
    }

    @Override
    public String toString() {
        return "Requete{" +
                "sujet='" + sujet + '\'' +
                ", description='" + description + '\'' +
                ", fichier=" + fichier +
                ", numero=" + numero +
                ", clientUsername='" + clientUsername + '\'' +
                ", techUsername='" + techUsername + '\'' +
                ", statut=" + statut +
                ", categorie=" + categorie +
                ", listeCommentaires=" + listeCommentaires +
                '}';
    }
}