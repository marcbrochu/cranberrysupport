package com.cranberrysupport.gui;

import com.cranberrysupport.bean.Commentaire;
import com.cranberrysupport.bean.Requete;
import com.cranberrysupport.bean.Technicien;
import com.cranberrysupport.bean.Utilisateur;
import com.cranberrysupport.service.BanqueRequetes;
import com.cranberrysupport.service.BanqueUtilisateurs;
import com.cranberrysupport.util.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;

public class TechFrame extends javax.swing.JFrame {

    private Utilisateur user;
    private ArrayList<Requete> listAssigne;
    private ArrayList<Requete> listDispo;
    private ArrayList<Utilisateur> listTechniciens;
    private Requete selectionner;
    private JFrame prec;
    private File file;
    private boolean showComments = false;

    public TechFrame(Utilisateur tech) {
        initComponents();
        this.setVisible(true);
        user = tech;
        listAssigne = BanqueRequetes.getRequeteAssignByTech(tech.getSurnom());
        listDispo = BanqueRequetes.getRequetesByStatut(Requete.Statut.ouvert);
        listTechniciens = BanqueUtilisateurs.getUsersByRole("technicien");

        fillModelAssign();
        fillModelRequeteOuverte();
        fillModelTech();
    }

    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        reqAssignedLbl = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        requeteAssigne = new javax.swing.JList();
        reqSelLbl = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requeteArea = new javax.swing.JTextArea();
        comLbl = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        commentsArea = new javax.swing.JTextArea();
        addComLbl = new javax.swing.JLabel();
        comIn = new javax.swing.JTextField();
        catBox = new javax.swing.JComboBox();
        reqDispoLbl = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        requeteDisponible = new javax.swing.JList();
        prendrereqBtn = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        descReqArea = new javax.swing.JTextArea();
        newRequeteBtn = new javax.swing.JButton();
        assignerReqBtn = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        supportList = new javax.swing.JList();
        finBox = new javax.swing.JComboBox();
        ajoutfichierBtn = new javax.swing.JButton();
        affRapportBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        fileLbl = new javax.swing.JLabel();

        setFrame();
        createListener();
        drawFrame();
    }
    private void setFrame() {
        reqAssignedLbl.setText("Les requêtes qui vous sont assignées:");
        reqSelLbl.setText("Requête sélectionnée:");
        comLbl.setText("Commentaires:");
        addComLbl.setText("Ajouter commentaire:");
        reqDispoLbl.setText("Requêtes disponibles non-assignée:");
        prendrereqBtn.setText("Prendre la requête sélectionnée");
        newRequeteBtn.setText("Créer une nouvelle requête");
        assignerReqBtn.setText("Assigner la requête à ce membre");
        ajoutfichierBtn.setText("Ajouter un fichier");
        affRapportBtn.setText("Afficher le rapport");
        quitBtn.setText("Quitter");
        fileLbl.setText("(pas de fichier attaché)");

        jScrollPane1.setViewportView(requeteAssigne);
        jScrollPane2.setViewportView(requeteArea);
        jScrollPane3.setViewportView(commentsArea);
        jScrollPane4.setViewportView(requeteDisponible);
        jScrollPane5.setViewportView(descReqArea);
        jScrollPane6.setViewportView(supportList);

        catBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
                "Changer la catégorie",
                Requete.Categorie.posteDeTravail.getValueString(),
                Requete.Categorie.serveur.getValueString(),
                Requete.Categorie.serviceWeb.getValueString(),
                Requete.Categorie.compteUsager.getValueString(),
                Requete.Categorie.autre.getValueString()})
        );

        finBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
                        Requete.Statut.finalSucces.getValueString(),
                        Requete.Statut.ouvert.getValueString(),
                        Requete.Statut.enTraitement.getValueString(),
                        Requete.Statut.finalAbandon.getValueString(),
                })
        );
    }
    private void createListener() {
        requeteAssigne.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                reqAssigneeValueChanged();
            }
        });

        catBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                catBoxItemStateChanged(evt);
            }
        });

        requeteDisponible.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                reqDispoValueChanged();
            }
        });

        prendrereqBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prendrereqBtnActionPerformed(evt);
            }
        });

        comIn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comInKeyPressed(evt);
            }
        });

        newRequeteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startNewRequeteFrame();
            }
        });

        assignerReqBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignerReqBtnActionPerformed();
            }
        });

        finBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                finBoxItemStateChanged();
            }
        });

        ajoutfichierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajouterFichier();
            }
        });

        affRapportBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startRapportFrame();
            }
        });

        quitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                System.exit(0);
            }
        });

        fileLbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fileLblMouseClicked();
            }
        });
    }
    private void drawFrame() {
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        requeteArea.setColumns(20);
        requeteArea.setRows(5);

        commentsArea.setColumns(20);
        commentsArea.setRows(5);

        descReqArea.setColumns(20);
        descReqArea.setRows(5);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reqDispoLbl)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane5)
                                    .addComponent(prendrereqBtn))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                                    .addComponent(assignerReqBtn, javax.swing.GroupLayout.Alignment.LEADING))))
                        .addGap(86, 86, 86))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                            .addComponent(reqAssignedLbl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reqSelLbl)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(comLbl)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(addComLbl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(comIn)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ajoutfichierBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fileLbl)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(quitBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(affRapportBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(newRequeteBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(newRequeteBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(affRapportBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(quitBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(reqAssignedLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(comLbl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(addComLbl)
                                        .addComponent(comIn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(ajoutfichierBtn)
                                        .addComponent(fileLbl)))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(reqSelLbl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(3, 3, 3)))
                .addGap(38, 38, 38)
                .addComponent(reqDispoLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(prendrereqBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(assignerReqBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }

    private void reqAssigneeValueChanged() {
        updateRequeteSelectionner();
        updateCommentaires();
    }

    private void reqDispoValueChanged() {

        if (selectionner != null)
            prendrereqBtn.setEnabled(true);
        else
            prendrereqBtn.setEnabled(false);

        if (!listDispo.isEmpty()) {
            updateRequeteD();
        }
    }

    private void catBoxItemStateChanged(java.awt.event.ItemEvent evt) {
        if (catBox.getSelectedIndex() > 0) {
            listAssigne.get(requeteAssigne.getSelectedIndex()).setCategorie(Requete.Categorie.fromString((String) catBox.getSelectedItem()));
            updateRequeteSelectionner();
        }
    }

    private void prendrereqBtnActionPerformed(java.awt.event.ActionEvent evt) {
        if (requeteDisponible.getSelectedIndex() >= 0) {
            Requete r = listDispo.get(requeteDisponible.getSelectedIndex());
            r.setTech(user);
            r.setStatut(Requete.Statut.enTraitement);

            listAssigne = BanqueRequetes.getRequeteAssignByTech(user.getSurnom());

            fillModelAssign();
            listDispo.remove(r);

            fillModelRequeteOuverte();
        }
    }

    private void fillModelAssign() {
        DefaultListModel modalListAssigne = new DefaultListModel();
        if (listAssigne.isEmpty()) {
            modalListAssigne.addElement("Vous n'avez pas de requête encore.");
        } else {
            for (Requete requete : listAssigne)
                modalListAssigne.addElement(requete.getSujet());
        }
        requeteAssigne.setModel(modalListAssigne);
    }

    private void fillModelRequeteOuverte() {
        DefaultListModel modalListRequete = new DefaultListModel();
        if (listDispo.isEmpty()) {
            modalListRequete.addElement("Il n'y a pas de requêtes pour le moment.");
        } else {
            for (Requete requete : listDispo) {
                modalListRequete.addElement(requete.getSujet());
            }
        }
        requeteDisponible.setModel(modalListRequete);
    }

    private void fillModelTech() {
        DefaultListModel modalListTech = new DefaultListModel();
        if (listTechniciens.isEmpty()) {
            modalListTech.addElement("Il n'y a pas de technicien");
        } else {
            for (Utilisateur utilisateur : listTechniciens) {
                modalListTech.addElement(utilisateur.getSurnom());
            }
        }
        supportList.setModel(modalListTech);
    }

    private void comInKeyPressed(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {

            BanqueRequetes.newCommentaire(comIn.getText(),user.getSurnom(), selectionner.getNumero());

            String s = "";
            for (Commentaire cmt : selectionner.getCommentaires()) {
                s += cmt.formattedComments();
                s += "\n";
            }
            commentsArea.setText(s);
            comIn.setText("");
        }
    }

    private void ajouterFichier() {
        fileChooser.showOpenDialog(prec);

        if (fileChooser.getSelectedFile() != null) {
            if (file.exists()) {
                Utils.saveFile(fileChooser.getSelectedFile());
                FichierAddOkFrame ok = new FichierAddOkFrame();
                ok.setVisible(true);
            }
        }

        listAssigne.get(requeteAssigne.getSelectedIndex()).setFile(file);

        updateRequeteSelectionner();
    }

    private void fileLblMouseClicked() {
        try {
            Requete req = listAssigne.get(requeteAssigne.getSelectedIndex());
            java.awt.Desktop dt = java.awt.Desktop.getDesktop();
            dt.open(req.getFichier());
        } catch (IOException ex) {
            Logger.getLogger(TechFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void finBoxItemStateChanged() {
        if (finBox.getSelectedIndex() > 0) {
            listAssigne.get(requeteAssigne.getSelectedIndex()).setStatut(Requete.Statut.fromString((String) finBox.getSelectedItem()));
            updateRequeteSelectionner();
        }
    }

    private void assignerReqBtnActionPerformed() {
        if (requeteDisponible.getSelectedIndex() >= 0) {
            Requete r = listDispo.get(requeteDisponible.getSelectedIndex());
            Technicien autre = (Technicien) listTechniciens.get(supportList.getSelectedIndex());
            r.setTech(autre);
            r.setStatut(Requete.Statut.enTraitement);

            listDispo.remove(r);

            fillModelRequeteOuverte();
        }
    }

    private void startNewRequeteFrame() {
        this.setVisible(false);
        NewRequeteFrame nouvelleRequete = new NewRequeteFrame(user, this);
    }

    private void startRapportFrame() {
        RapportFrame rapport = new RapportFrame(BanqueRequetes.getAllTechInfo());
        rapport.setVisible(true);
    }

    private void updateRequeteSelectionner() {
        if (requeteAssigne.getSelectedIndex() >= 0) {
            selectionner = listAssigne.get(requeteAssigne.getSelectedIndex());
            requeteArea.setText("Sujet: " + selectionner.getSujet()
                    + "\nDescription: " + selectionner.getDescription()
                    + "\nCatégorie: " + selectionner.getCategorie().toString()
                    + "\nStatut: " + selectionner.getStatut().toString());

            updateCommentaireRequeteSelectionner();

            showFilePath();

        } else {
            requeteArea.setText("");
            commentsArea.setText("");
        }
        if (selectionner.getStatut() == Requete.Statut.enTraitement) {
            finBox.setEnabled(true);
        } else {
            finBox.setEnabled(false);
        }
    }

    private void updateCommentaireRequeteSelectionner() {
        String comments = "";
        for (Commentaire cmt : selectionner.getCommentaires()) {
            comments += cmt.formattedComments();
            comments += "\n";
        }
        commentsArea.setText(comments);
    }

    private void showFilePath() {
        if (selectionner.getFichier() != null && selectionner.getFichier().exists()) {
            fileLbl.setVisible(true);
            fileLbl.setText("Afficher " + selectionner.getFichier().getPath());
        } else {
            fileLbl.setVisible(false);
        }
    }

    private void updateRequeteD() {
        if (requeteDisponible.getSelectedIndex() >= 0) {
            selectionner = listDispo.get(requeteDisponible.getSelectedIndex());
            descReqArea.setText("Sujet: " + selectionner.getSujet()
                    + "\nDescription: " + selectionner.getDescription()
                    + "\nCatégorie: " + selectionner.getCategorie().toString()
                    + "\nStatut: " + selectionner.getStatut().toString());
        } else {
            descReqArea.setText("");
        }
    }

    private void updateCommentaires() {
        if (selectionner != null) {
            if (!showComments) {
                String s = "";
                for (Commentaire c : selectionner.getCommentaires()) {
                    s += c.getAuteur() + ": " + c.getComment() + "\n";
                }
                commentsArea.setText(s);
                showComments = true;
            }
        }
    }

    private javax.swing.JLabel addComLbl;
    private javax.swing.JButton affRapportBtn;
    private javax.swing.JButton ajoutfichierBtn;
    private javax.swing.JButton assignerReqBtn;
    private javax.swing.JComboBox catBox;
    private javax.swing.JTextField comIn;
    private javax.swing.JLabel comLbl;
    private javax.swing.JTextArea commentsArea;
    private javax.swing.JTextArea descReqArea;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel fileLbl;
    private javax.swing.JComboBox finBox;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JButton newRequeteBtn;
    private javax.swing.JButton prendrereqBtn;
    private javax.swing.JButton quitBtn;
    private javax.swing.JLabel reqAssignedLbl;
    private javax.swing.JList requeteAssigne;
    private javax.swing.JList requeteDisponible;
    private javax.swing.JLabel reqDispoLbl;
    private javax.swing.JLabel reqSelLbl;
    private javax.swing.JTextArea requeteArea;
    private javax.swing.JList supportList;
}
