package com.cranberrysupport.gui;

import com.cranberrysupport.bean.Utilisateur;
import com.cranberrysupport.service.BanqueUtilisateurs;
import com.cranberrysupport.util.Utils;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LoginFrame extends javax.swing.JFrame {
	String type;

    public LoginFrame(String type) {
    	this.type = type;
        initComponents();
        setVisible(true);
    }
    
    private void initComponents() {

        nameLbl = new javax.swing.JLabel();
        mdpLbl = new javax.swing.JLabel();
        exName = new javax.swing.JLabel();
        exMdp = new javax.swing.JLabel();
        utilisateur = new javax.swing.JTextField();
        mdp = new javax.swing.JPasswordField();
        okBtn = new javax.swing.JButton();
        annulerBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        initLabelText();

        initBtnOk();
        initBtnCancel();

        initGroupLayout();

        pack();
    }

	private void initGroupLayout() {
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(okBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(annulerBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nameLbl)
                            .addComponent(mdpLbl))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mdp, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
                            .addComponent(utilisateur, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(exMdp)
                    .addComponent(exName))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameLbl)
                    .addComponent(utilisateur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exName))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mdpLbl)
                    .addComponent(mdp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exMdp))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(annulerBtn)
                    .addComponent(okBtn))
                .addContainerGap(20, Short.MAX_VALUE))
        );
	}

	private void initLabelText() {
		nameLbl.setText("Nom d'utilisateur:");

        mdpLbl.setText("Mot de passe:");

        if(type.equals(Utilisateur.ROLE_CLIENT)) {
        	exName.setText("ex.: Isabeau Desrochers");
            exMdp.setText("ex.: MonMdp");
        }
	}

	private void initBtnCancel() {
		annulerBtn.setText("Annuler");
        annulerBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                annulerBtnMouseClicked(evt);
            }
        });
	}

	private void initBtnOk() {
		okBtn.setText("OK");
        okBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                okBtnMouseClicked(evt);
            }
        });
	}

    private void okBtnMouseClicked(java.awt.event.MouseEvent evt) {
        String nomUtil = utilisateur.getText();
        String motdepasse = Utils.sha256(mdp.getText());

        Utilisateur potentiel = BanqueUtilisateurs.loginUtilisateur(nomUtil,motdepasse);

        if(potentiel != null && potentiel.getRole().equals(this.type)){
            this.setVisible(false);
            if(this.type.equals(Utilisateur.ROLE_CLIENT)) {
            	ClientFrame fenetreClient = new ClientFrame(potentiel);            	            	
            }
            else {
            	TechFrame fenetreTech = new TechFrame(potentiel);
            }
        } else {
            ErrorLoginFrame mauvaisLogin = new ErrorLoginFrame(this.type);
            this.setVisible(false);
        }
    }

    private void annulerBtnMouseClicked(java.awt.event.MouseEvent evt) {
        this.setVisible(false);
        CranberryFrame revenir = new CranberryFrame();
    }

    private javax.swing.JButton annulerBtn;
    private javax.swing.JLabel exMdp;
    private javax.swing.JLabel exName;
    private javax.swing.JTextField mdp;
    private javax.swing.JLabel mdpLbl;
    private javax.swing.JLabel nameLbl;
    private javax.swing.JButton okBtn;
    private javax.swing.JTextField utilisateur;
}
