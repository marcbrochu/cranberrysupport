package com.cranberrysupport.gui;

import com.cranberrysupport.bean.Commentaire;
import com.cranberrysupport.bean.Requete;
import com.cranberrysupport.bean.Utilisateur;
import com.cranberrysupport.service.BanqueRequetes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ClientFrame extends javax.swing.JFrame {

    private Utilisateur user;
    private ArrayList<Requete> listUserRequete;
    private Requete selectRequete;
    private JFrame prec;
    private File file;
    private boolean showComments = false;

    public ClientFrame(Utilisateur client) {
        initComponents();
        this.setVisible(true);
        user = client;
        listUserRequete = BanqueRequetes.getRequetesByUsername(client.getSurnom());

        initListeRequete();

        commentArea.setVisible(false);
    }

    private void initListeRequete() {
        DefaultListModel listRequete = new DefaultListModel();

        if (listUserRequete.isEmpty()) {
            listRequete.addElement("Vous n'avez pas de requête encore.");
        } else {
            for (Requete requete:listUserRequete) {
                listRequete.addElement(requete.getSujet());
            }
        }
        listRequeteList.setModel(listRequete);
    }

    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        listRequeteList = new javax.swing.JList();
        requetesLbl = new javax.swing.JLabel();
        addRequeteBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        modifBtn = new javax.swing.JButton();
        fichierBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requeteArea = new javax.swing.JTextArea();
        voirComsBtn = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        commentArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        initRequestList();

        initLabels();

        initBtnNewRequest();
        initBtnQuit();
        initBtnAddComment();
        initBtnAddFile();
        initBtnSeeComments();

        initRequeteArea();
        initCommentArea();
     
        initLayouts();

        pack();
    }

	private void initRequestList() {
		listRequeteList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "bouba" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listRequeteList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listRequeteListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(listRequeteList);
	}

	private void initLabels() {
		requetesLbl.setText("Vos requêtes:");
        jLabel1.setText("Modifier la requête sélectionnée:");
	}

	private void initLayouts() {
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addRequeteBtn)
                    .addComponent(requetesLbl)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(quitBtn))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(voirComsBtn)
                            .addComponent(fichierBtn)
                            .addComponent(modifBtn)
                            .addComponent(jLabel1)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(addRequeteBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(requetesLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(quitBtn))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(voirComsBtn)
                                .addGap(26, 26, 26)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fichierBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(modifBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)))))
                .addContainerGap())
        );
	}

	private void initCommentArea() {
		commentArea.setColumns(20);
        commentArea.setLineWrap(true);
        commentArea.setRows(5);
        commentArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                commentAreaKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(commentArea);
	}

	private void initBtnSeeComments() {
		voirComsBtn.setBackground(new java.awt.Color(102, 153, 255));
        voirComsBtn.setText("Voir les commentaires");
        voirComsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voirComsBtnActionPerformed(evt);
            }
        });
	}

	private void initRequeteArea() {
		requeteArea.setColumns(20);
        requeteArea.setEditable(false);
        requeteArea.setLineWrap(true);
        requeteArea.setRows(5);
        jScrollPane2.setViewportView(requeteArea);
	}

	private void initBtnAddFile() {
		fichierBtn.setText("Ajouter un fichier");
        fichierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fichierBtnActionPerformed(evt);
            }
        });
	}

	private void initBtnAddComment() {
		modifBtn.setText("Ajouter un commentaire");
        modifBtn.setEnabled(false);
        modifBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifBtnActionPerformed(evt);
            }
        });
	}
	
	private void initBtnQuit() {
		quitBtn.setText("Quitter");
        quitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });
	}

	private void initBtnNewRequest() {
		addRequeteBtn.setText("Faire une nouvelle requête");
        addRequeteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRequeteBtnActionPerformed(evt);
            }
        });
	}
    
    private void addRequeteBtnActionPerformed(java.awt.event.ActionEvent evt) {
        this.setVisible(false);
        NewRequeteFrame nouvelleRequete = new NewRequeteFrame(user, this);
    }

    private void listRequeteListValueChanged(javax.swing.event.ListSelectionEvent evt) {
        updateRequete();
    }

    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {
        System.exit(0);
    }

    private void fichierBtnActionPerformed(java.awt.event.ActionEvent evt) {
        fileChooser.showOpenDialog(prec);
        file = fileChooser.getSelectedFile();
        try {
            listUserRequete.get(listRequeteList.getSelectedIndex()).setFile(file);
        } catch (IndexOutOfBoundsException e) {
        }

        File f = fileChooser.getSelectedFile();
        if (f != null) {
            try {
                FileWriter newFile = new FileWriter("dat/" + f.getName());
            } catch (IOException ex) {
                Logger.getLogger(NewRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (file.exists()) {
                FichierAddOkFrame ok = new FichierAddOkFrame();
                ok.setVisible(true);
            }
        }
    }

    private void modifBtnActionPerformed(java.awt.event.ActionEvent evt) {
        commentArea.setVisible(true);
        commentArea.requestFocus();
    }

    private void voirComsBtnActionPerformed(java.awt.event.ActionEvent evt) {
        if (selectRequete != null) {
            if (!showComments) {
                String s = "";
                for (Commentaire c : selectRequete.getCommentaires()) {
                    s += c.formattedComments();
                }
                requeteArea.setText(s);
                showComments = true;
                voirComsBtn.setText("Voir la rêquete");
                fichierBtn.setEnabled(false);
                modifBtn.setEnabled(true);
            } else {
                updateRequete();
            }
        }
    }

    private void updateRequete() {
        selectRequete = listUserRequete.get(listRequeteList.getSelectedIndex());
        requeteArea.setText("Sujet: " + selectRequete.getSujet()
                + "\nDescription: " + selectRequete.getDescription()
                + "\nCatégorie: " + selectRequete.getCategorie().toString()
                + "\nStatut: " + selectRequete.getStatut().toString());
        showComments = false;
        voirComsBtn.setText("Voir les commentaires");
        fichierBtn.setEnabled(true);
        modifBtn.setEnabled(false);
    }

    private void commentAreaKeyPressed(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {

            BanqueRequetes.newCommentaire(commentArea.getText(),user.getSurnom(), selectRequete.getNumero());

            String s = "";
            for (Commentaire c : selectRequete.getCommentaires()) {
                s += c.toString();
            }
            requeteArea.setText(s);
            commentArea.setVisible(false);
            modifBtn.requestFocus();
        }
    }

    private javax.swing.JButton addRequeteBtn;
    private javax.swing.JTextArea commentArea;
    private javax.swing.JButton fichierBtn;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JList listRequeteList;
    private javax.swing.JButton modifBtn;
    private javax.swing.JButton quitBtn;
    private javax.swing.JTextArea requeteArea;
    private javax.swing.JLabel requetesLbl;
    private javax.swing.JButton voirComsBtn;
}
