package com.cranberrysupport.gui;

import com.cranberrysupport.bean.Requete;
import com.cranberrysupport.bean.Utilisateur;
import com.cranberrysupport.service.BanqueRequetes;
import com.cranberrysupport.util.Utils;

import javax.swing.JFrame;

public class NewRequeteFrame extends javax.swing.JFrame {

    private Utilisateur user;
    private JFrame precFrame;

    public NewRequeteFrame(Utilisateur user, JFrame pagePrecedente) {
        initComponents();
        this.setVisible(true);
        this.user = user;
        this.precFrame = pagePrecedente;
    }

    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        titleLbl = new javax.swing.JLabel();
        sujetLbl = new javax.swing.JLabel();
        sujetFld = new javax.swing.JTextField();
        descripLbl = new javax.swing.JLabel();
        descpScroll = new javax.swing.JScrollPane();
        descpArea = new javax.swing.JTextArea();
        fichierLbl = new javax.swing.JLabel();
        filePathField = new javax.swing.JTextField();
        selectFileBtn = new javax.swing.JButton();
        categorieListBox = new javax.swing.JComboBox();
        catLbl = new javax.swing.JLabel();
        saveAndQuitBtn = new javax.swing.JButton();
        closeFrameBtn = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        setComposante();
        createListener();
        drawFrame();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    }
    private void setComposante() {
        titleLbl.setText("Nouvelle requête");
        fichierLbl.setText("Fichier:");
        sujetLbl.setText("Sujet de la requête:");
        selectFileBtn.setText("Téléverser un fichier");
        descripLbl.setText("Description:");
        catLbl.setText("Catégorie:");
        saveAndQuitBtn.setText("Terminer");
        closeFrameBtn.setText("Revenir");

        categorieListBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
                "Poste de travail", "Serveur", "Service web", "Compte usager", "Autre" }));
        filePathField.setEditable(false);
    }
    private void createListener() {
        selectFileBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileChooser.showOpenDialog(precFrame);
                filePathField.setText(fileChooser.getSelectedFile().getPath());

                Utils.saveFile(fileChooser.getSelectedFile());
            }
        });

        saveAndQuitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveRequete();
                closeFrame(evt);
            }
        });

        closeFrameBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeFrame(evt);
            }
        });
    }
    private void drawFrame() {
        descpArea.setColumns(20);
        descpArea.setRows(5);
        descpArea.setBorder(null);
        descpScroll.setViewportView(descpArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(titleLbl)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(sujetLbl)
                                    .addComponent(descripLbl)
                                    .addComponent(fichierLbl))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(filePathField, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                    .addComponent(sujetFld, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                    .addComponent(descpScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                    .addComponent(selectFileBtn))))
                        .addContainerGap(30, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(catLbl)
                        .addGap(63, 63, 63)
                        .addComponent(categorieListBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(176, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(124, 124, 124)
                        .addComponent(saveAndQuitBtn)
                        .addGap(18, 18, 18)
                        .addComponent(closeFrameBtn)
                        .addContainerGap(104, Short.MAX_VALUE))))
            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLbl)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sujetLbl)
                    .addComponent(sujetFld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(descripLbl)
                    .addComponent(descpScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(filePathField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fichierLbl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(selectFileBtn)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(categorieListBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(catLbl))
                .addGap(27, 27, 27)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(closeFrameBtn)
                    .addComponent(saveAndQuitBtn))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        pack();
    }

    private void saveRequete() {
        BanqueRequetes.newRequete(
                sujetFld.getText(),
                descpArea.getText().replaceAll("\n", " "),
                user,
                Requete.Categorie.fromString((String) categorieListBox.getSelectedItem()),
                fileChooser.getSelectedFile()
        );
    }

    private void closeFrame(java.awt.event.ActionEvent evt) {
        this.setVisible(false);
        precFrame.setVisible(true);
    }

    private javax.swing.JComboBox categorieListBox;
    private javax.swing.JLabel catLbl;
    private javax.swing.JTextArea descpArea;
    private javax.swing.JScrollPane descpScroll;
    private javax.swing.JLabel descripLbl;
    private javax.swing.JButton saveAndQuitBtn;
    private javax.swing.JLabel fichierLbl;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField filePathField;
    private javax.swing.JButton closeFrameBtn;
    private javax.swing.JButton selectFileBtn;
    private javax.swing.JTextField sujetFld;
    private javax.swing.JLabel sujetLbl;
    private javax.swing.JLabel titleLbl;
}
