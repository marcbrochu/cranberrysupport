package com.cranberrysupport.gui;

import com.cranberrysupport.bean.Utilisateur;

public class ErrorLoginFrame extends javax.swing.JFrame {

    private static final String WRONGMDP = "Désolé, votre nom d'utilisateur ou votre mot de passe est incorrect.";

    private String errorMsg;
    private String role;

    public ErrorLoginFrame(String role) {
    	this.role = role;
        this.errorMsg = WRONGMDP;
        initComponents();
        this.setVisible(true);
    }

    public ErrorLoginFrame(String role, String errorMsg) {
    	this.role = role;
        this.errorMsg = errorMsg;
        initComponents();
        this.setVisible(true);
    }

    private void initComponents() {

        dslLbl = new javax.swing.JLabel();
        revenirLbl = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        dslLbl.setText(errorMsg);

        revenirLbl.setText("Recommencer");
        revenirLbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                revenirLblMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(81, Short.MAX_VALUE)
                .addComponent(dslLbl)
                .addGap(72, 72, 72))
            .addGroup(layout.createSequentialGroup()
                .addGap(184, 184, 184)
                .addComponent(revenirLbl)
                .addContainerGap(194, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(dslLbl)
                .addGap(18, 18, 18)
                .addComponent(revenirLbl)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        pack();
    }

    private void revenirLblMouseClicked(java.awt.event.MouseEvent evt) {
        this.setVisible(false);
        LoginFrame nouvelEssai = new LoginFrame(role);
    }

    private javax.swing.JLabel dslLbl;
    private javax.swing.JButton revenirLbl;
}
