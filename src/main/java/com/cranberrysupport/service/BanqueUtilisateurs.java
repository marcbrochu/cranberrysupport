package com.cranberrysupport.service;

import com.cranberrysupport.bean.Utilisateur;
import com.cranberrysupport.sql.QueryManager;

import java.util.ArrayList;

public class BanqueUtilisateurs {
    private static ArrayList<Utilisateur> utilisateurs;
    private static QueryManager queryManager;

    public static void loadUtilisateur() {
        utilisateurs = new ArrayList<>();
        queryManager = new QueryManager();
        utilisateurs = queryManager.fetchUsers();
    }

    public static Utilisateur loginUtilisateur(String surnom, String motdp) {
        for(Utilisateur utilisateur:utilisateurs)
            if(utilisateur.getSurnom().equalsIgnoreCase(surnom) && utilisateur.getMdp().equals(motdp))
                return utilisateur;

        return null;
    }

    public static ArrayList<Utilisateur> getUsersByRole(String role) {
        ArrayList<Utilisateur> users = new ArrayList<>();

        for (Utilisateur utilisateur:utilisateurs)
            if (utilisateur.getRole().equals(role))
                users.add(utilisateur);

        return users;
    }

    //On assume qu'il n'y a qu'un Utilisateur par nom d'utilisateur
    public static Utilisateur getUserByUserName(String nomVoulu) {
        for (Utilisateur utilisateur: utilisateurs)
            if (utilisateur.getSurnom().equalsIgnoreCase(nomVoulu))
                return utilisateur;
        return null;
    }
}