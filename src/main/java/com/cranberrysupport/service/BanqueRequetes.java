package com.cranberrysupport.service;

import com.cranberrysupport.bean.Commentaire;
import com.cranberrysupport.bean.Requete;
import com.cranberrysupport.bean.Technicien;
import com.cranberrysupport.bean.Utilisateur;
import com.cranberrysupport.sql.QueryManager;

import java.io.File;
import java.util.ArrayList;

public class BanqueRequetes {
    private static ArrayList<Requete> listeRequetes;
    private static QueryManager queryManager;
    private static int compteur = 0;

    public static void loadRequetes() {
        listeRequetes = new ArrayList<>();
        queryManager = new QueryManager();
        listeRequetes = queryManager.fetchRequete();
        compteur = listeRequetes.get(listeRequetes.size()-1).getNumero();
    }

    public static void newRequete(String sujet, String desrcrip, Utilisateur client, Requete.Categorie cat, File file) {
        compteur++;
        Requete nouvelle = new Requete(compteur, sujet, desrcrip, client.getSurnom(), cat);

        nouvelle.setFile(file);

        listeRequetes.add(nouvelle);
        queryManager.insertRequete(nouvelle);
    }

    public static void newCommentaire(String msg, String surnom, int requeteId) {
        Commentaire commentaire = new Commentaire(msg,surnom);

        queryManager.insertCommentaire(commentaire, requeteId);

        for(Requete requete:listeRequetes)
            if(requete.getNumero() == requeteId)
                requete.addCommentaire(msg, surnom);
    }


    public static ArrayList<Requete> getRequetesByStatut(Requete.Statut statut) {
        ArrayList<Requete> statutRequete = new ArrayList<>();
        for (Requete requete:listeRequetes)
            if (requete.getStatut().equals(statut))
                statutRequete.add(requete);
        return statutRequete;
    }

    public static ArrayList<Requete> getRequetesByUsername(String username) {
        ArrayList<Requete> statutRequete = new ArrayList<>();
        for (Requete requete:listeRequetes)
            if (requete.getClient().equalsIgnoreCase(username))
                statutRequete.add(requete);
        return statutRequete;
    }

    public static ArrayList<Requete> getRequeteAssignByTech(String username) {
        ArrayList<Requete> statutRequete = new ArrayList<>();
        for (Requete requete:listeRequetes)
            if (requete.getTechnicien().equalsIgnoreCase(username))
                statutRequete.add(requete);
        return statutRequete;
    }

    public static String getAllTechInfo() {
        String rapport = "";
        for (Utilisateur tempo: BanqueUtilisateurs.getUsersByRole(Utilisateur.ROLE_TECHNICIEN)) {
            rapport += tempo.getSurnom() + ":\n";
            rapport += getRequeteParStatut((Technicien) tempo) + "\n";
        }
        return rapport;
    }

    private static String getRequeteParStatut(Technicien tech) {
        int ouv = 0, tr = 0, su = 0, ab = 0;
        ArrayList<Requete> requeteByTech = getRequeteAssignByTech(tech.getSurnom());

        for (Requete requete : requeteByTech) {
            if (requete.getStatut().equals(Requete.Statut.ouvert) &&
                    requete.getTechnicien().equals(tech.getSurnom()))
                ouv++;
            else if (requete.getStatut().equals(Requete.Statut.enTraitement) &&
                    requete.getTechnicien().equals(tech.getSurnom()))
                tr++;
            else if (requete.getStatut().equals(Requete.Statut.finalSucces) &&
                    requete.getTechnicien().equals(tech.getSurnom()))
                su++;
            else if (requete.getStatut().equals(Requete.Statut.finalAbandon) &&
                    requete.getTechnicien().equals(tech.getSurnom()))
                ab++;
        }

        return "Statut ouvert: " + ouv + "\n"
                + "Statut En traitement: " + tr + "\n"
                + "Statut succes: " + su + "\n"
                + "Statut abandonnée: " + ab + "\n";
    }

}
