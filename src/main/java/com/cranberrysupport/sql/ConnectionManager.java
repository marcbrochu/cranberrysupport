package com.cranberrysupport.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private static final String DATABASEURL = "jdbc:sqlite:dat/cranberryDB.db";
    private Connection connection = null;

    /**
     * @return (True) Connection established
     *          (False) Not connected to database
     */
    public boolean connect() {
        try {
            connection = DriverManager.getConnection(DATABASEURL);
            System.out.println("Connection to CranberryDB has been established.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * @return (True) Connection close
     *          (False) Not connected to database
     */
    public boolean disconnect() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public Connection getConnection() {
        return connection;
    }
}
