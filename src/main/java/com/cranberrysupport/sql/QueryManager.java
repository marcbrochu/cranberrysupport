package com.cranberrysupport.sql;

import com.cranberrysupport.bean.*;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class QueryManager {
    private ConnectionManager connectionManager;
    private static final String SELECTUSERS = "SELECT * FROM Utilisateur";
    private static final String SELECTREQUETE = "SELECT * FROM Requete";
    private static final String SELECTCOMMENTAIREBYID = "SELECT * FROM Commentaire WHERE requete_id=";

    private static final String INSERTREQUETE = "INSERT INTO Requete" +
            "(numero, sujet, description, fichier, client, technicien, statut, categorie) " +
            "VALUES(?,?,?,?,?,?,?,?)";
    private static final String INSERTCOMMENTAIRE = "INSERT INTO Commentaire" +
            "(commentaire, auteur, requete_id) " +
            "VALUES(?,?,?)";

    public QueryManager() {
        connectionManager = new ConnectionManager();
    }

    public ArrayList<Utilisateur> fetchUsers(){
        ArrayList<Utilisateur> utilisateurs = new ArrayList<>();
        try{
            connectionManager.connect();
            Statement stmt = connectionManager.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(SELECTUSERS);

            while (rs.next()) {
                if (rs.getString("role").equals(Utilisateur.ROLE_CLIENT)) {
                    utilisateurs.add(new Client(
                            rs.getString("surnom"),
                            rs.getString("mdp"),
                            rs.getString("role")));
                }
                else if (rs.getString("role").equals(Utilisateur.ROLE_TECHNICIEN)) {
                    utilisateurs.add(new Technicien(
                            rs.getString("surnom"),
                            rs.getString("mdp"),
                            rs.getString("role")));
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            connectionManager.disconnect();
        }
        return utilisateurs;
    }

    public ArrayList<Requete> fetchRequete() {
        ArrayList<Requete> requetes = new ArrayList<>();
        try{
            connectionManager.connect();
            Statement stmt = connectionManager.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(SELECTREQUETE);

            requetes = createListRequetes(rs);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connectionManager.disconnect();
        }

        for(Requete requete:requetes) {
            System.out.println(requete);
        }


        return requetes;
    }

    private ArrayList<Requete> createListRequetes(ResultSet rs) throws SQLException, IOException {
        ArrayList<Requete> requetes = new ArrayList<>();

        while (rs.next()) {

            Requete requete = new Requete(
                    rs.getString("sujet"),
                    rs.getString("description"),
                    rs.getString("fichier"),
                    rs.getInt("numero"),
                    rs.getString("client"),
                    rs.getString("technicien"),
                    Requete.Statut.fromString(rs.getString("statut")),
                    Requete.Categorie.fromString(rs.getString("categorie")),
                    fetchCommentaireByRequeteId(rs.getInt("numero")));

            requetes.add(requete);
        }
        return requetes;
    }

    private ArrayList<Commentaire> fetchCommentaireByRequeteId(int requeteNumber) {
        ArrayList<Commentaire> commentaires = new ArrayList<>();

        try{
            Statement stmt = connectionManager.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(SELECTCOMMENTAIREBYID+requeteNumber);

            while (rs.next()) {
                commentaires.add(new Commentaire(
                        rs.getString("commentaire"),
                        rs.getString("auteur")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return commentaires;
    }

    public void insertRequete(Requete requete) {
        try {
            connectionManager.connect();

            PreparedStatement ps = connectionManager.getConnection().prepareStatement(INSERTREQUETE);

            ps.setInt(1, requete.getNumero());
            ps.setString(2, requete.getSujet());
            ps.setString(3, requete.getDescription());
            ps.setString(4, requete.getFichierPath());
            ps.setString(5, requete.getClient());
            ps.setString(6, requete.getTechnicien());
            ps.setString(7, requete.getStatut().getValueString());
            ps.setString(8, requete.getCategorie().getValueString());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.disconnect();
        }
    }

    public void insertCommentaire(Commentaire commentaire, int requeteId) {
        try {
            connectionManager.connect();

            PreparedStatement ps = connectionManager.getConnection().prepareStatement(INSERTCOMMENTAIRE);

            ps.setString(1, commentaire.getComment());
            ps.setString(2, commentaire.getAuteur());
            ps.setInt(3, requeteId);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.disconnect();
        }
    }
}
