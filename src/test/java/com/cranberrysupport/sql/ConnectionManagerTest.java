package com.cranberrysupport.sql;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;

import static org.junit.Assert.*;

public class ConnectionManagerTest {

    private ConnectionManager connectionManager;

    @Before
    public void setUp() throws Exception {
        connectionManager = new ConnectionManager();
    }

    @After
    public void tearDown() throws Exception {
        connectionManager = null;
    }

    @Test
    public void connect() throws Exception {
        assertTrue(connectionManager.connect());
    }

    @Test
    public void disconnect() throws Exception {
        connectionManager.connect();
        assertTrue(connectionManager.disconnect());
        assertNull(connectionManager.getConnection());
    }

    @Test
    public void getConnection() throws Exception {
        connectionManager.connect();
        assertNotNull(connectionManager.getConnection());
    }

}