package com.cranberrysupport.sql;

import com.cranberrysupport.bean.Requete;
import com.cranberrysupport.bean.Utilisateur;
import com.cranberrysupport.util.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class QueryManagerTest {
    private QueryManager queryManager;

    @Before
    public void setUp() throws Exception {
        queryManager = new QueryManager();
    }

    @After
    public void tearDown() throws Exception {
        queryManager = null;
    }

    @Test
    public void fetchUsers() throws Exception {
        ArrayList<Utilisateur> utilisateurs = queryManager.fetchUsers();
        assertEquals("marco", utilisateurs.get(0).getSurnom());
        assertEquals(Utils.sha256("123"), utilisateurs.get(0).getMdp());

        assertEquals("martinejodoin", utilisateurs.get(utilisateurs.size()-1).getSurnom());
    }

    @Test
    public void fetchRequete() throws Exception {
        ArrayList<Requete> requetes = queryManager.fetchRequete();

        assertEquals("Exemple 1", requetes.get(0).getSujet());
        assertEquals(Requete.Statut.ouvert, requetes.get(0).getStatut());

        assertEquals("marco", requetes.get(2).getClient());
    }

}