package com.cranberrysupport.bean;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TechnicienTest {
	
	Technicien technicien;
	
    @Before
    public void setUp() throws Exception {
    	technicien = new Technicien("smoakF", "secret", Utilisateur.ROLE_TECHNICIEN);
    }

    @After
    public void tearDown() throws Exception {
    	technicien = null;
    }

    @Test
    public void getRole() throws Exception {
    	assertTrue(technicien.getRole().equals(Utilisateur.ROLE_TECHNICIEN));
    }
}