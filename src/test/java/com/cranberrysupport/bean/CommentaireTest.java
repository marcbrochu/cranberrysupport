package com.cranberrysupport.bean;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommentaireTest {
	
	Commentaire commentaire;
	
    @Before
    public void setUp() throws Exception {
    	commentaire = new Commentaire("Ceci est un commentaire", "Marc-Andre");
    }

    @After
    public void tearDown() throws Exception {
    	commentaire = null;
    }

    @Test
    public void formattedComments() throws Exception {
    	assertTrue(commentaire.formattedComments().equals(commentaire.getAuteur() + ": " + commentaire.getComment()));
    }

    @Test
    public void testToString() throws Exception {
    	assertTrue(commentaire.toString().equals("Commentaire{" +
                "commentaire='" + commentaire.getComment() + '\'' +
                ", auteur=" + commentaire.getAuteur() +
                '}'));
    }

}