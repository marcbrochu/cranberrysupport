package com.cranberrysupport.bean;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.File;

public class RequeteTest {
	
	Requete requete;
	Technicien technicien;
	Client client;
	String message;
	File file;
	
    @Before
    public void setUp() throws Exception {
    	requete = new Requete(1, "Sujet", "description", "clientUsername", Requete.Categorie.autre);
    	technicien = new Technicien("tech", "secret", Utilisateur.ROLE_TECHNICIEN);
    	client = new Client("client", "secret", Utilisateur.ROLE_CLIENT);
    	message = "Ceci est un message pertinant";
    	file = new File("path");
    }

    @After
    public void tearDown() throws Exception {
    	requete = null;
    	technicien = null;
    	client = null;
    	message = null;
    	file = null;
    }

    @Test
    public void addCommentaire() throws Exception {
    	requete.addCommentaire(message, technicien.getSurnom());
    	assertTrue(requete.getCommentaires().size() == 1);
    	assertTrue(requete.getCommentaires().get(0).getComment().equals(message));
    }
    
    @Test
    public void setTech() throws Exception {
    	requete.setTech(technicien);
    	assertTrue(requete.getTechnicien().equals("tech"));
    }
    
    @Test
    public void setAClientAsTech() throws Exception {
    	requete.setTech(client);
    	assertTrue(requete.getTechnicien().equals(""));
    }
    
    @Test
    public void getFichierPath() throws Exception {
    	assertTrue(requete.getFichierPath().equals(""));
    	requete.setFile(file);
    	assertTrue(requete.getFichierPath().equals("path"));
    }
        
}