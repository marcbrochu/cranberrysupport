package com.cranberrysupport.bean;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClientTest {
	
	Client client;
	
    @Before
    public void setUp() throws Exception {
    	client = new Client("graton.bob", "secret", Utilisateur.ROLE_CLIENT);
    }

    @After
    public void tearDown() throws Exception {
    	client = null;
    }

    @Test
    public void getRole() throws Exception {
    	assertTrue(client.getRole().equals(Utilisateur.ROLE_CLIENT));
    }
}