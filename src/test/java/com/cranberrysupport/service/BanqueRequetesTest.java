package com.cranberrysupport.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.cranberrysupport.bean.Requete;

import static org.junit.Assert.*;

import java.util.ArrayList;

public class BanqueRequetesTest {
	
    @Before
    public void setUp() throws Exception {
    	BanqueRequetes.loadRequetes();	
    }

    @Test
    public void testGetRequetesByStatut() throws Exception {
    	ArrayList<Requete> listeReq = BanqueRequetes.getRequetesByStatut(Requete.Statut.enTraitement);
    	for(Requete req: listeReq) {
    		assertTrue(req.getStatut().equals(Requete.Statut.enTraitement));
    	}
    }
    
    @Test
    public void testGetRequetesByUsername() throws Exception {
    	ArrayList<Requete> listeReq = BanqueRequetes.getRequetesByUsername("popoline");
    	for(Requete req: listeReq) {
    		assertTrue(req.getClient().equals("popoline"));
    	}
    }
    
    @Test
    public void testGetRequeteAssignByTech() throws Exception {
    	ArrayList<Requete> listeReq = BanqueRequetes.getRequeteAssignByTech("techguest");
    	for(Requete req: listeReq) {
    		assertTrue(req.getTechnicien().equals("techguest"));
    	}
    }
}