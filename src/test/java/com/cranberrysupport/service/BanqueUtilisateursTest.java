package com.cranberrysupport.service;

import org.junit.Before;
import org.junit.Test;

import com.cranberrysupport.bean.Utilisateur;
import com.cranberrysupport.util.Utils;

import static org.junit.Assert.*;

import java.util.ArrayList;

public class BanqueUtilisateursTest {
	
    @Before
    public void setUp() throws Exception {
    	BanqueUtilisateurs.loadUtilisateur();
    }
    
    @Test
    public void loginUtilisateur() throws Exception {
    	 Utilisateur user = BanqueUtilisateurs.loginUtilisateur("popoline", Utils.sha256("monchatkiki"));
    	 System.out.println(user);
    	 assertTrue(user.getSurnom().equals("popoline"));
    }

    @Test
    public void getUsersByRole() throws Exception {
    	ArrayList<Utilisateur> listeUser = BanqueUtilisateurs.getUsersByRole(Utilisateur.ROLE_CLIENT);
    	for(Utilisateur user: listeUser) {
    		assertTrue(user.getRole().equals(Utilisateur.ROLE_CLIENT));
    	}
    }

    @Test
    public void getUserByUserName() throws Exception {
    	Utilisateur user = BanqueUtilisateurs.getUserByUserName("popoline");
    	assertTrue(user.getSurnom().equals("popoline"));
    }

}